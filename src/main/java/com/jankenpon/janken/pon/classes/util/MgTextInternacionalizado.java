package com.jankenpon.janken.pon.classes.util;

import java.util.Locale;
import java.util.ResourceBundle;



public class MgTextInternacionalizado {

	private static String nmBundle = "Labels";

	public static String getTxtInternacionalizado(String key) {
		ResourceBundle bundle = getResourceBundle();
		try {
			return bundle.getString(key);
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public static ResourceBundle getResourceBundle() {

		return ResourceBundle.getBundle(nmBundle, getLocale());
	}

	public static Locale getLocale() {
		return Locale.getDefault ();
	}



}
