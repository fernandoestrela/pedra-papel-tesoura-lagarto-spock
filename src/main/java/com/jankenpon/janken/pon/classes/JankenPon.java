/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jankenpon.janken.pon.classes;

/**
 *
 * @author Fernando Estrela
 */
public enum JankenPon {
    PAPEL(1),//
    TEZOURA(2),//
    LAGARTO(3),//
    PEDRA(4),//
    SPOCK(5);
    
    int jogada;
    JankenPon(int jogada){
        this.jogada = jogada; 
    }
    public int  get(){
        return jogada;
    }
}
