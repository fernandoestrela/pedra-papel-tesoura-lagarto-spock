/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jankenpon.janken.pon.classes;

/**
 *
 * @author Fernando Estrela
 */
public class Jogador {
    private String nome;
    private int pontos;
    private JankenPon jankenPon;
    private boolean vencedorRodada;
    private boolean vencdorPartida;
    private boolean jogadorVez;

    public boolean isVencdorPartida() {
        return vencdorPartida;
    }

    public void setVencdorPartida(boolean vencdorPartida) {
        this.vencdorPartida = vencdorPartida;
    }

    public boolean isVencedorRodada() {
       return vencedorRodada;
    }

    public void setVencedorRodada(boolean vencedorRodada) {
        this.vencedorRodada = vencedorRodada;
    }

    public JankenPon getJankenPon() {
        return jankenPon;
    }

    public void setJankenPon(JankenPon jankenPon) {
        this.jankenPon = jankenPon;
    }

  

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getPontos() {
        return pontos;
    }

    public void setPontos(int pontos) {
        this.pontos = pontos;
    }

    public boolean isJogadorVez() {
        return jogadorVez;
    }

    public void setJogadorVez(boolean jogadorVez) {
        this.jogadorVez = jogadorVez;
    }
    
    public void incrementaPontos(){
        this.pontos++;
    }
    
    
}
