/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jankenpon.janken.pon.classes;

/**
 *
 * @author Fernando Estrela
 */
public class Jogo {
    private Jogador jogador1;
    private Jogador jogador2;
    private Jogador jogadorVencedor;
    private int qtdRodada;
    private int rodada;
    private static final int TOTAL_RODADA = 3;

  
    
    public Jogo(){
        this.jogador1 = new Jogador();
        this.jogador2 = new Jogador();
    }

    public int getRodada() {
        return rodada;
    }

    public void setRodada(int rodada) {
        this.rodada = rodada;
    }
    
    
    public Jogador getJogador1() {
        return jogador1;
    }

    public int getQtdRodada() {
        return qtdRodada;
    }

    public void setQtdRodada(int qtdRodada) {
        this.qtdRodada = qtdRodada;
    }
    public void setJogador1(Jogador jogador1) {
        this.jogador1 = jogador1;
    }

    public Jogador getJogador2() {
        return jogador2;
    }

    public void setJogador2(Jogador jogador2) {
        this.jogador2 = jogador2;
    }
    
    public void verificaRodada(){
        if(jogador1.getJankenPon().get() == JankenPon.TEZOURA.get() &&
                (jogador2.getJankenPon().get() == JankenPon.PAPEL.get() ||
                jogador2.getJankenPon().get() == JankenPon.LAGARTO.get() )){
            
            jogador1.setVencedorRodada(true);
            jogador1.incrementaPontos();
            jogador2.setVencedorRodada(false);
        }else if(jogador2.getJankenPon().get() == JankenPon.TEZOURA.get() &&
                (jogador1.getJankenPon().get() == JankenPon.PAPEL.get() ||
                jogador1.getJankenPon().get() == JankenPon.LAGARTO.get() )){
            
            jogador2.setVencedorRodada(true);
            jogador2.incrementaPontos();
            jogador1.setVencedorRodada(false);
        }else if(jogador1.getJankenPon().get() == JankenPon.PAPEL.get() &&
                (jogador2.getJankenPon().get() == JankenPon.PEDRA.get() ||
                        jogador2.getJankenPon().get() == JankenPon.SPOCK.get())){
            
            jogador1.setVencedorRodada(true);
            jogador1.incrementaPontos();
            jogador2.setVencedorRodada(false);
        }else if(jogador2.getJankenPon().get() == JankenPon.PAPEL.get() &&
                (jogador1.getJankenPon().get() == JankenPon.PEDRA.get() ||
                        jogador1.getJankenPon().get() == JankenPon.SPOCK.get())){
            
            jogador2.setVencedorRodada(true);
            jogador2.incrementaPontos();
            jogador1.setVencedorRodada(false);
        }else if(jogador1.getJankenPon().get() == JankenPon.PEDRA.get()&&
                (jogador2.getJankenPon().get() == JankenPon.LAGARTO.get() ||
                jogador2.getJankenPon().get() == JankenPon.TEZOURA.get() )){
            
            jogador1.setVencedorRodada(true);
            jogador1.incrementaPontos();
            jogador2.setVencedorRodada(false);
        }else if(jogador2.getJankenPon().get() == JankenPon.PEDRA.get()&&
                (jogador1.getJankenPon().get() == JankenPon.LAGARTO.get() ||
                jogador1.getJankenPon().get() == JankenPon.TEZOURA.get() )){
            
            jogador2.setVencedorRodada(true);
            jogador2.incrementaPontos();
            jogador1.setVencedorRodada(false);
        }else if(jogador1.getJankenPon().get() == JankenPon.LAGARTO.get()&&
                (jogador2.getJankenPon().get() == JankenPon.SPOCK.get() ||
                jogador2.getJankenPon().get() == JankenPon.PAPEL.get() )){
            
            jogador1.setVencedorRodada(true);
            jogador1.incrementaPontos();
            jogador2.setVencedorRodada(false);
        }else if(jogador2.getJankenPon().get() == JankenPon.LAGARTO.get()&&
                (jogador1.getJankenPon().get() == JankenPon.SPOCK.get() ||
                jogador1.getJankenPon().get() == JankenPon.PAPEL.get() )){
            
            jogador2.setVencedorRodada(true);
            jogador2.incrementaPontos();
            jogador1.setVencedorRodada(false);
        } else if(jogador1.getJankenPon().get() == JankenPon.SPOCK.get()&&
                (jogador2.getJankenPon().get() == JankenPon.TEZOURA.get() ||
                jogador2.getJankenPon().get() == JankenPon.PEDRA.get() )){
            
            jogador1.setVencedorRodada(true);
            jogador1.incrementaPontos();
            jogador2.setVencedorRodada(false);
        }else if(jogador2.getJankenPon().get() == JankenPon.SPOCK.get()&&
                (jogador1.getJankenPon().get() == JankenPon.TEZOURA.get() ||
                jogador1.getJankenPon().get() == JankenPon.PEDRA.get() )){
            
            jogador2.setVencedorRodada(true);
            jogador2.incrementaPontos();
            jogador1.setVencedorRodada(false);
        }
    }
    
    public void verificaVencedor(){
        if(jogador1.getPontos() > jogador2.getPontos()){
            jogador1.setVencdorPartida(true);
            this.setJogadorVencedor(jogador1);
        }else{
            jogador2.setVencdorPartida(true);
            this.setJogadorVencedor(jogador2);
        }
            
    }

    public Jogador getJogadorVencedor() {
        return jogadorVencedor;
    }

    public void setJogadorVencedor(Jogador jogadorVencedor) {
        this.jogadorVencedor = jogadorVencedor;
    }
    
    
   public boolean verificaFimJogo(){
       return getRodada() == TOTAL_RODADA;
   }
   
   public boolean isRodada(){
       return qtdRodada == 2;
   }
    
    public void incrementaRodada(){
        this.rodada++;
    }
    
     public void incrementaQtdRodada(){
        this.qtdRodada++;
    }
}
