package com.jankenpon.janken.pon;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.jankenpon.janken.pon.classes.JankenPon;
import com.jankenpon.janken.pon.classes.Jogo;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author User
 */
public class TesteJankPon {
    
    public TesteJankPon() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    
     @Test
     public void testarVerificaRodadaPapelTezoura() {
        Jogo jogo = new Jogo();
        jogo.getJogador1().setJankenPon(JankenPon.PAPEL);
        jogo.getJogador2().setJankenPon(JankenPon.TEZOURA);
        jogo.verificaRodada();
         assertTrue(jogo.getJogador2().isVencedorRodada());
     }
     
      @Test
     public void testarVerificaRodadaLagartoSpock() {
        Jogo jogo = new Jogo();
        jogo.getJogador1().setJankenPon(JankenPon.LAGARTO);
        jogo.getJogador2().setJankenPon(JankenPon.SPOCK);
        jogo.verificaRodada();
         assertTrue(jogo.getJogador1().isVencedorRodada());
     }
     
      @Test
     public void testarVerificaVencedor() {
        Jogo jogo = new Jogo();
        jogo.getJogador1().setJankenPon(JankenPon.LAGARTO);
        jogo.getJogador2().setJankenPon(JankenPon.SPOCK);
        jogo.verificaRodada();
        jogo.getJogador1().setJankenPon(JankenPon.TEZOURA);
        jogo.getJogador2().setJankenPon(JankenPon.PAPEL);
        jogo.verificaRodada();
        jogo.getJogador1().setJankenPon(JankenPon.PEDRA);
        jogo.getJogador2().setJankenPon(JankenPon.TEZOURA);
        jogo.verificaRodada();
        jogo.verificaVencedor();
         assertTrue(jogo.getJogador1().isVencdorPartida());
     }
     
       @Test
     public void testarIsRodada() {
        Jogo jogo = new Jogo();
        jogo.setQtdRodada(1);
         assertFalse(jogo.isRodada());
     }
}
